<?php

/**
 * @file: class_portfolio.module
 * Allows students to add their completed and graded assignments to a portfolio.
 *
 * @author: Jared <jluxenbe@andrew.cmu.edu>
 * @copyright: 2004-2007 MyClassroomHelper
 * @license: GNU - See LICENCE.TXT
*/

/*
Attribution:
This module been donated to the Drupal community by Teachers Without Borders,
who received a grant from Cisco Philanthropy to create a suite of
content and collaboration tools for teachers.
Teachers Without Borders (http://www.teacherswithoutborders.org) hired Osoft (http://www.osoft.com)
to develop the TWB toolset, available for free for TWB Members.
Please join TWB (it's free) in order to provide feedback
and help us improve the TWB Toolset.
For inquiries, contact Fred Mednick, Founder of Teachers Without Borders:
fred@teacherswithoutborders.org
*/

/*
Note: simple table to inner join with nodes table to get a listing of student's 
portfolio pieces.
*/

//==================================================================

/**
 * Implementation of hook_nodeapi
 */
function class_portfolio_nodeapi(&$node, $op, $teaser, $page)
{
	global $user;
	
	// if the type is assignment, force class_portfolio if the 'force add' flag is set
	if($node->type == 'assignment' && $node->src_node->class_extra_properties['class_portfolio_force_add'])
	{
		$node->class_portfolio = true;
	}
	
	// portfolio ops
	switch($op)
	{
		case 'load':
			$node->class_portfolio = (db_num_rows(db_query('SELECT nid FROM class_portfolio WHERE nid = %d', $node->nid)) > 0);
			
			break;
			
		case 'update':
			if(!$node->class_portfolio)
			{
				$result = db_num_rows(db_query('SELECT nid FROM {class_portfolio} WHERE nid = %d', $node->nid));
				if($result) drupal_set_message("Removed '{$node->title}' from your portfolio.");
				db_query('DELETE FROM {class_portfolio} WHERE nid = %d', $node->nid);
			}
			
		case 'insert':
			if($node->class_portfolio && !db_num_rows(db_query('SELECT nid FROM {class_portfolio} WHERE nid = %d', $node->nid)))
			{
				drupal_set_message("Added '{$node->title}' to your portfolio.");
				db_query('INSERT INTO {class_portfolio} VALUES (%d, %d, %d)', 
					$node->nid, 0, 0);
			}
			break;
			
		case 'delete':
			db_query('DELETE FROM {class_portfolio} WHERE nid = %d', $node->nid);
			break;
	}
}

/** 
 * Implementation of hook_menu()
 */
function class_portfolio_menu($may_cache)
{
	global $user;
	
	$items = array();
	
	if(!$may_cache)
	{
		if(arg(0) == 'node' && is_numeric(arg(1)))
		{
			$node = node_load(arg(1));
			
			//if((($node->type == 'assignment' && $node->finished && !is_null($node->grade)) || arg(2) == 'add_to_portfolio') && $node->uid == $user->uid)
			
			if($node->type == 'assigned_node' && arg(2) == 'add_to_portfolio')
			{
				//print_r($node);
				if($node->assignment_nid)  // if this is assigned work, redirect to the appropriate place
				{
					//print('node/' . $node->assignment_nid . '/add_to_portfolio');
					$qs = array();
					foreach($_GET as $k => $v)
					{
						if($k != 'q')
						$qs[] = "$k=$v";
					}
					unset($_REQUEST['destination']);
					drupal_goto('node/' . $node->assignment_nid . '/add_to_portfolio', implode('&',$qs));
				}
				else
				{
					drupal_set_message(t('You can only put assigned work in your portfolio after you have completed the assignment'), 'error');
					drupal_goto($_GET['destination']);
				}
			}
			
			if(class_portfolio_types($node->type) && $node->uid == $user->uid)
			{
				
				
				if(!$node->class_portfolio)
				{
					$items[] = array(
						'path' => 'node/' . arg(1) . '/add_to_portfolio',
						'title' => t('add to portfolio'),
						'access' => user_access('add to portfolio') && class_get_user_role($nid) != 'teacher',
						'type' => MENU_LOCAL_TASK,
						'callback' => 'class_portfolio_add_to_portfolio',
						'callback arguments' => array(arg(1))
					);
				}
				else
				{
					$items[] = array(
						'path' => 'node/' . arg(1) . '/add_to_portfolio',
						'title' => t('remove from portfolio'),
						'access' => user_access('add to portfolio') && class_get_user_role($nid) != 'teacher',
						'type' => MENU_LOCAL_TASK,
						'callback' => 'class_portfolio_add_to_portfolio',
						'callback arguments' => array(arg(1))
					);
				}
			}
		}
	}
	
	return $items;
}

/**
 * Implementation of hook_perm
 */
function class_portfolio_perm()
{
	return( array('add to portfolio') );
}

/**
 * Implementation of hook_views_tables
 */
function class_portfolio_views_tables()
{
	$tables['node_class_portfolio_ops'] = array(
		'name' => 'node',
		'join' => array(
			'left' => array(
				'table' => 'node',
				'field' => 'nid',
			),
			'right' => array(
				'field' => 'nid'
			)
		),
		'fields' => array(
			'nid' => array(
				'name' => t('CLASS Portfolio: Operations'),
				'field' => 'nid',
				'handler' => array(
					'class_portfolio_views_field_handler_operations' => t('Show add to/remove from portfolio')
				),
				'addlfields' => array('type')
			),
		)
	);
	
	$tables['node_class_portfolio'] = array(
		'name' => 'class_portfolio',
		'join' => array(
			'left' => array(
				'table' => 'node',
				'field' => 'nid'
			),
			'right' => array(
				'field' => 'nid'
			)
		),
		/*'fields' => array(
			'nid' => array(
				//'field' => 'nid',
				'name' => t('CLASS Portfolio: Operations'),
				'handler' => array(
					'class_portfolio_views_field_handler_operations' => t('Show add to/remove from portfolio')
				),
				//'addlfields' => array('nid')
			)
		),*/
		'filters' => array(
			'in_portfolio' => array(
				'field' => 'nid',
				'name' => t('CLASS Portfolio: In Portfolio'),
				'handler' => 'class_portfolio_views_handler',
				'operator' => array('=' => t('Is Equal To')),
				'list' => array('true' => t('True')),
				'list-type' => 'select',
				'help' => t('Filters nodes based on whether or not they are in a user\'s portfolio')
			)
		)
	);
	
	$tables['class_portfolio_required'] = array(
		'name' => 'node_properties',
		'join' => array(
			'left' => array(
				'table' => 'assigned_nodes',
				'field' => 'nid'
			),
			'right' => array(
				'field' => 'nid'
			),
			'extra' => array(
				'k' => 'class_portfolio_force_add'
			)
		),
		'fields' => array(
			'nid' => array(
				'name' => t('CLASS Portfolio: Force Add to Portfolio'),
				'handler' => 'class_portfolio_handler_field_portfolio_item',
				'sortable' => true,
				'help' => t('This will indicate whether the node is a required portfolio item or not.')
			)
		),
		'filters' => array(
			'copyable' => array(
				'field' => 'vid',
				'name' => t('CLASS Portfolio: Force Add to Portfolio'),
				'handler' => 'class_portfolio_handler_portfolio_item_filter',
				'operator' => array('=' => t('Is Equal To')),
				'value' => array('#type' => 'select', '#options' => array('either' => t('either'), 'required' => t('required'), 'not required' => t('not required'))),
				'help' => t('Filters nodes based on whether or not they are required portfolio items or not.')
			)
		)
	);
	
	return $tables;
}

function class_portfolio_handler_field_portfolio_item($fieldinfo, $fielddata, $value, $data)
{
	return theme('class_portfolio_portfolio_item', $value);
}

function theme_class_portfolio_portfolio_item($value)
{
	return $value ? t('yes') : t('no');
}

function class_portfolio_handler_portfolio_item_filter($op, $filter, $filterinfo, &$query) 
{
	if($filter['value'] == 'required') {
		$query->add_field('nid', 'class_portfolio_required', 'class_portfolio_required_nid');
		$query->add_where('class_portfolio_required.k = \'%s\'', 'class_portfolio_force_add');
	} else if($filter['value'] == 'not required') {
		$query->add_field('nid', 'class_portfolio_required', 'class_portfolio_required_nid');
		$query->add_where('class_portfolio_required.k is NULL');
	}
}

/**
 * Implementation of hook_form_alter
 *
 * add a 'must add to portfolio' property to any assignment type's node
 * editing form
 */
function class_portfolio_form_alter($form_id, &$form) {
  if (isset($form['type']) && $form['type']['#value'] .'_node_form' == $form_id && class_assignment_assignment_type($form['type']['#value'])) {
	if($form['nid'] && !empty($form['nid']['#value']))
	{
		$nid = $form['nid']['#value'];
		if($form['vid'] && !empty($form['vid']['#value']))
		{
			$vid = $form['vid']['#value'];
		}
		$node = node_load($nid,$vid);
	}
	class_extra_properties_add($form, 'class_portfolio_force_add', array(
      '#type' => 'checkbox',
      '#title' => t('Automatically add this assignment to a student\'s portfolio upon completion?'),
      '#return_value' => 'yes',
      '#default_value' => !!($node->class_extra_properties['class_portfolio_force_add'])
    ));
  }
}

function class_portfolio_views_field_handler_operations($fieldinfo, $fielddata, $value, $data)
{
	/*print_r($fieldinfo);
	print_r($fielddata);
	print_r("<BR>$value<BR>");
	print_r($data);*/
	/*$value = $data->node_class_portfolio_nid;
	$nid = $data->nid;*/
	
	//print_r($data);
	
	/*if($value && class_portfolio_types($data->type))
	{*/
		if($data->node_class_portfolio_nid)
			return l(t('remove'), 'node/' . $value . '/add_to_portfolio', NULL, 'destination=' . $_GET['q']);
		else
			return l(t('add'), 'node/' . $value . '/add_to_portfolio', NULL, 'destination=' . $_GET['q']);
	/*}
	else
	{
		return t('n/a');
	}*/
}

function class_portfolio_views_handler($op, $filter, $filterinfo, &$query) 
{
	// inner join the class_portfolio table to filter
	$tablename = 'node';
	$query->add_table('class_portfolio', false, 1, array( 'type' => 'inner', 'left' => array( 'table' => $tablename, 'field' => 'nid' ), 'right' => array('field' => 'nid')));
	$query->add_field('weight', 'class_portfolio');
	$query->add_field('flag', 'class_portfolio');
}

function class_portfolio_add_to_portfolio($nid)
{
	$form = array();
	$form['nid'] = array('#type' => 'value', '#value' => $nid);
	
	$node = node_load($nid);
	
	if($node->class_portfolio)
	{
		$form['add'] = array('#type' => 'value', '#value' => false);
		$description = t('Are you sure you want to remove this assignment from your portfolio?');
		$title = t('remove from portfolio');
	}
	else
	{
		$form['add'] = array('#type' => 'value', '#value' => true);
		$description = t('Are you sure you want to add this assignment to your portfolio?');
		$title = t('add to portfolio');
	}
	
	return confirm_form('class_portfolio_add_to_portfolio', $form, $title, 'node/' . $nid, $description);
}

function class_portfolio_add_to_portfolio_submit($form_id, $form_values)
{
	if($form_values['confirm'])
	{
		$node = node_load($form_values['nid']);
		$node->class_portfolio = $form_values['add'];
		
		node_save($node);
	}
	if($_REQUEST['destination'] == 'class/myportfolio')
	{
		return 'class/myportfolio';
	}
	else
	{
		return 'node/' . $node->nid;
	}
}

/**
 * Implementation of hook_settings().
 */
function class_portfolio_settings()
{
	$form = array();
	$valid_types = class_portfolio_types();

	drupal_set_title("Assign nodes settings");
	
	$node_types = node_get_types();
	$form['prompt'] = array( '#value' => '<p>' . t('Select the node types that are valid for addition to users\' portfolios.'). '</p>' );
	$form['class_portfolio_valid_node_types'] = array( '#tree' => true );
	
	// add checkboxes for the node types
	foreach($node_types as $t => $name) {
		$default = in_array($t, $valid_types);
		$form['class_portfolio_valid_node_types'][$t] = array(
			'#type' => 'checkbox',
			'#title' => $name,
			'#default_value' => $default,
			'#return_value' => $t
		);
	}
	
	$form['array_filter'] = array('#type' => 'value', '#value' => TRUE);
	return $form;
}

function class_portfolio_types($item = NULL)
{
	$valid_types = variable_get('class_portfolio_valid_node_types', array());
	
	if(is_null($item)) return $valid_types;
	return in_array($item, $valid_types);
}

function class_portfolio_user($op, $edit, &$account, $category = NULL)
{
	switch($op) {
    case 'view':
        return array(t('Portfolio') => array(
				array('value' => l(t('View my portfolio'), 'class_portfolio_public/' . $account->uid), 'title' => '', 'class' => 'og_groups')
			)
		);
	break;
	}
}

/**
 * Implementation of hook_help
 */
function class_portfolio_help($section)
{
	switch($section) {
		case 'admin/modules#description':
			return t('This module provides a portfolio for students to catalog their work.');
	}
}

/** 
 * Implementation of hook_views_default_views
 */
function class_portfolio_views_default_views()
{
//------------------------------------------------------------------------------------
  $view = new stdClass();
  $view->name = 'class_student_portfolio';
  $view->description = 'A listing of the items in a students\' portfolio.';
  $view->access = array (
  0 => '2',
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Portfolio';
  $view->page_header = 'The following items are in your portfolio.';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'You have no items in your portfolio.
';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'class_student_portfolio';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '10';
  $view->sort = array (
  );
  $view->argument = array (
    array (
      'type' => 'gid',
      'argdefault' => '4',
      'title' => '%1 Portfolio',
      'options' => '',
      'wildcard' => 'any',
      'wildcard_substitution' => 'all classes',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => 'Title',
      'handler' => 'views_handler_field_nodelink',
      'sortable' => '1',
      'options' => 'link',
    ),
    array (
      'tablename' => 'og_node_data',
      'field' => 'title',
      'label' => 'Class',
    ),
    array (
      'tablename' => 'node',
      'field' => 'type',
      'label' => 'Type',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'created',
      'label' => 'Created On',
      'handler' => 'views_handler_field_date_small',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'og_uid_node',
      'field' => 'currentuid',
      'operator' => '=',
      'options' => '',
      'value' => '***CURRENT_USER***',
    ),
    array (
      'tablename' => 'class_node',
      'field' => 'currentteacher',
      'operator' => '=',
      'options' => '',
      'value' => 'true',
    ),
    array (
      'tablename' => 'node_class_portfolio',
      'field' => 'in_portfolio',
      'operator' => '=',
      'options' => '',
      'value' => 'true',
    ),
    array (
      'tablename' => 'node',
      'field' => 'currentuid',
      'operator' => '=',
      'options' => '',
      'value' => '***CURRENT_USER***',
    ),
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(node, og_node_data, og_uid_node, class_node, node_class_portfolio);
  $views[$view->name] = $view;

//------------------------------------------------------------------------------------
  $view = new stdClass();
  $view->name = 'class_teacher_portfolio';
  $view->description = 'A listing of the items in a teachers students portfolios.';
  $view->access = array (
  0 => '2',
);
  $view->view_args_php = '';
  $view->page = TRUE;
  $view->page_title = 'Portfolio';
  $view->page_header = 'The following items are in your students portfolios.';
  $view->page_header_format = '1';
  $view->page_footer = '';
  $view->page_footer_format = '1';
  $view->page_empty = 'You have no items in your portfolio.
';
  $view->page_empty_format = '1';
  $view->page_type = 'table';
  $view->url = 'class_teacher_portfolio';
  $view->use_pager = TRUE;
  $view->nodes_per_page = '10';
  $view->sort = array (
  );
  $view->argument = array (
    array (
      'type' => 'gid',
      'argdefault' => '6',
      'title' => '',
      'options' => '',
      'wildcard' => '',
      'wildcard_substitution' => '',
    ),
  );
  $view->field = array (
    array (
      'tablename' => 'node',
      'field' => 'title',
      'label' => 'Title',
      'handler' => 'views_handler_field_nodelink_with_mark',
      'sortable' => '1',
      'options' => 'link',
    ),
    array (
      'tablename' => 'og_node_data',
      'field' => 'title',
      'label' => 'Class',
    ),
    array (
      'tablename' => 'node',
      'field' => 'type',
      'label' => 'Type',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'created',
      'label' => 'Created On',
      'handler' => 'views_handler_field_date_small',
      'sortable' => '1',
    ),
    array (
      'tablename' => 'node',
      'field' => 'changed',
      'label' => 'Updated On',
      'handler' => 'views_handler_field_since',
      'sortable' => '1',
      'defaultsort' => 'ASC',
    ),
  );
  $view->filter = array (
    array (
      'tablename' => 'og_uid_node',
      'field' => 'currentuid_admin',
      'operator' => '=',
      'options' => '',
      'value' => '***CURRENT_USER***',
    ),
    array (
      'tablename' => 'node_class_portfolio',
      'field' => 'in_portfolio',
      'operator' => '=',
      'options' => '',
      'value' => 'true',
    ),
    array (
      'tablename' => 'class_node',
      'field' => 'currentteacher',
      'operator' => '=',
      'options' => '',
      'value' => 'true',
    ),
  );
  $view->exposed_filter = array (
  );
  $view->requires = array(node, og_node_data, og_uid_node, node_class_portfolio, class_node);
  $views[$view->name] = $view;

//------------------------------------------------------------------------------------
  return $views;
}